#include "caesarCypher.h"

int main() //driver code
{
	int s; //variable for storing the value of the shift provided by the input file
	string k; //variable for storing the alphabet in a random sequenced
	ifstream infile;
	infile.open("caesar.d1"); //opens a file that contains the key, a verison of the alphabet in random order, and shift value.
	if (!infile)
	{
		cerr << "Unable to open file.";
		exit(EXIT_FAILURE);   // stops program if file does not open
	}
	while (infile >> s >> k) 	// Read the entire infile.
	{
		process_infile(s, k);
	}
	infile.close(); //closes input file when done reading
	return 0;
}

void process_infile(const int& shift, const string& key) //Function establishes a connection to the stream and reads in the data from the text file.
{
	string s, t; //string variables for storing a line from input file (s), and for displaying that line after encryption (t)
	ifstream infile;
	infile.open("caesar.d2"); //opens the file meant to be encrypted
	if (!infile)
	{
		cerr << "Unable to open file.";
		exit(EXIT_FAILURE);   // stops program if file does not open
	}
	cout << "\nshift = " << shift << endl; //display shift and key value for the user
	cout << "key = \"" << key << "\"" << endl << endl;
	while (!infile.eof())
	{
		getline(infile, s); //reads the file that is to be encrypted line by line and stores in variable string s
		t = encodeCaesarCipher(s, shift, key); //t is set to the encrypted line
		cout << t << endl; //displays the encrypted line for the user
	}
	infile.close(); //closes the input file
}


string encodeCaesarCipher(string str, const int& shift, const string& key)
{
	string newString = str; //copies the contents of the string for safety, the string is just what was read in from getline
	char temp; //declares a temp char that will be used to store the current char in the string
	int position = 0; 
	for (int i = 0; i < (int)newString.length(); i++) //increment through the string
	{
		temp = newString[i];
		if (isalpha(temp)) //This cypher will not affect anything that is not a letter. Spaces, numbers, or punctuation will stay in the exact same place.
		{
			position = new_position(temp, shift, key);
			for (int i = 0; i < abs(position); i++) //the following code block takes the position returned by new_position and loops through the alphabet for the given char, changing its value
			{
				if (position > 0) //if the position int returned by new_position is a positive integer the cypher iterates through the alphabet in a positive direction
				{
					if (temp >= 'a' && temp <= 'z') // iterates through the alphabet in a positive direction if the char is a lowercase letter, changing its ASCII value
					{
						if (temp == 'z') //moves to 'a' if the end of the alphabet is reached
						{
							temp = 'a';
						}
						else //if somewhere in the middle of the alphabet, increments
						{
							temp++;
						}
					}
					if (temp >= 'A' && temp <= 'Z') // iterates through the alphabet in a positive direction if the char is an uppercase letter
					{
						if (temp == 'Z') //moves to 'A' if the end of the alphabet is reached
						{
							temp = 'A';
						}
						else //if somewhere in the middle of the alphabet, increments
						{
							temp++;
						}
					}
				}
				if (position < 0) //iterates through the alphabet in a negative direction if the int returned by new_position is a negative integer
				{
					if (temp >= 'a' && temp <= 'z') // iterates through the alphabet in a negative direction if the char is a lowercase letter, changing its ASCII value
					{
						if (temp == 'a') //moves to 'z' if the beginning of the alphabet is reached
						{
							temp = 'z'; 
						}
						else //if somewhere in the middle of the alphabet, decrements
						{
							temp--;
						}
					}
					if (temp >= 'A' && temp <= 'Z') //iterates through the alphabet in a negative direction if the char is an uppercase letter, changing its ASCII value
					{
						if (temp == 'A') //moves to 'Z' if the beginning of the alphabet is reached
						{
							temp = 'Z';
						}
						else //if somewhere in the middle of the alphabet, decrements
						{
							temp--;
						}
					}
				}

			}
			newString[i] = temp; // assigns the encrypted temp char value to the position of the string
		}
	}
	return newString;
}


/* This function performs a second shift to encrypt the passage further. It takes the shift value and changes it based on the value of the alphabetical key provided. For example, the key 
can equal "QWERTYUIOPASDFGHJKLZXCVBNM". For the letter A, the corresponding letter in the key is Q, so the second shift value for A 
is -16. It does this for the full key and returns and int representing the second shift. This shift will then be applied to each char in the lines being read in from the input file */

int new_position(const char& c, const int& shift, const string& key) //returns the value of the second shift
{
	string alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	char a = toupper(c); //only deals with uppercase letters
	int newShiftValue = 0;
	int alphaPos = 0; //this variable is necessary to compare the value of the actual alphabet to the value of the key
	char x, y; //blank chars for the subtraction operation to get the new shift value.

	for (int i = 0; i < 26; i++) 
	{
		if (a == alphabet[i]) //if the character in the key matches the character in the alphabet
		{
			alphaPos = i; //the value of alphaPos char is its position in the string "ABCDEFGHIJKLMNOPQRSTUVWXYZ".
		}
	}
	x = key[alphaPos]; //char x is equal to the char at index position alphaPos in the KEY provided
	y = alphabet[alphaPos]; //char y is equal to the char at index position alphaPos in the ALPHABET
	newShiftValue = shift + (y - x); //subtract the value of the alphabet position from the value of the key position to get the new shift value
	return newShiftValue;
}