#ifndef H_PROG 
#define H_PROG 
#include <iostream>
#include <fstream>
#include <string>
#include <cstdlib>

using namespace std;

void process_infile(const int& shift, const string& key); //Function establishes a connection to the stream and reads in the data from the text file.

string encodeCaesarCipher(string str, const int& shift, const string& key); //this function takes the return value of new_position and shifts the char in the string char by char.

int new_position(const char& c, const int& shift, const string& key); //returns the value of the new shift based on the given shift and key

#endif